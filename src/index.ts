import _ from "lodash";
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import { Episode } from "@alline/model";
import { EpisodeContext, EpisodeResult, EpisodeScraper } from "@alline/core";
import { AsyncSeriesWaterfallHook } from "tapable";
import { resolve } from "url";

export interface RequestEpisodeScraperOption<T, U> {
  axios?: AxiosRequestConfig;
  transform: (value: U) => Promise<T>;
  label: string;
}

export interface RequestEpisodeScraperHook<T, U> {
  transformURL: AsyncSeriesWaterfallHook<[string, EpisodeContext]>;
  transformEpisode: AsyncSeriesWaterfallHook<[Episode, RequestContext<T, U>]>;
  transformThumbnails: AsyncSeriesWaterfallHook<
    [string[], RequestContext<T, U>]
  >;
}

export interface RequestContext<T, U> extends EpisodeContext {
  url: string;
  res: AxiosResponse<U>;
  value: T;
  resolveUrl: (path?: string) => string;
}

export class RequestEpisodeScraper<T = any, U = any> extends EpisodeScraper {
  requestHooks: RequestEpisodeScraperHook<T, U>;

  protected axios: AxiosInstance;
  protected option: RequestEpisodeScraperOption<T, U>;

  constructor(option: RequestEpisodeScraperOption<T, U>) {
    const { axios: axiosOption } = option;
    super();
    this.option = option;
    this.axios = axios.create(axiosOption);
    this.requestHooks = {
      transformURL: new AsyncSeriesWaterfallHook(["url", "ctx"]),
      transformEpisode: new AsyncSeriesWaterfallHook(["rlt", "ctx"]),
      transformThumbnails: new AsyncSeriesWaterfallHook(["thumbnails", "ctx"])
    };
  }

  protected async onScrap(
    rlt: EpisodeResult,
    ctx: EpisodeContext
  ): Promise<EpisodeResult> {
    const { logger } = ctx;
    const { transform, label } = this.option;
    const {
      transformEpisode,
      transformURL,
      transformThumbnails
    } = this.requestHooks;
    const url = await transformURL.promise("", ctx);
    const res = await this.axios.get<U>(encodeURI(url));
    const value = await transform(res.data);
    const requestContext = {
      url,
      res,
      value,
      resolveUrl: (path = "") => resolve(url, path),
      ...ctx
    };
    const [data, thumbnails] = await Promise.all([
      transformEpisode.promise(rlt.data, requestContext),
      transformThumbnails.promise([], requestContext)
    ]);
    logger.debug("onScrap", { label, data, thumbnails });
    return _.merge({}, rlt, { data, thumbnails });
  }
}
