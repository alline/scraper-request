# [2.0.0](https://gitlab.com/alline/scraper-request/compare/v1.2.0...v2.0.0) (2020-07-09)


### Bug Fixes

* update dependencies ([956d477](https://gitlab.com/alline/scraper-request/commit/956d477deba92d6e02f82b547fc66cc6c030d40a))


### BREAKING CHANGES

* update @alline/core to v3

# [1.2.0](https://gitlab.com/alline/scraper-request/compare/v1.1.0...v1.2.0) (2020-07-02)


### Bug Fixes

* change private variable to protect ([4aba6cb](https://gitlab.com/alline/scraper-request/commit/4aba6cb879b79fcceb541dba1b4813f030eeb8c0))


### Features

* add label to logger ([af2e8d3](https://gitlab.com/alline/scraper-request/commit/af2e8d300e470a7f586fc05ab8a162a6cdb2381c))

# [1.1.0](https://gitlab.com/alline/scraper-request/compare/v1.0.0...v1.1.0) (2020-07-02)


### Features

* add extension hooks ([88797b9](https://gitlab.com/alline/scraper-request/commit/88797b945009470e5c10ee4be981d25cf1fe7841))

# 1.0.0 (2020-07-01)


### Features

* initial commit ([09cc8c8](https://gitlab.com/alline/scraper-request/commit/09cc8c8412c04e4b1f1a8476fec86045823b0bf6))
